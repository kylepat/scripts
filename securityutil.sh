#!/bin/bash

#this script is design to install and activate basic server security utilities
#this script was made assuming the use of an Ubuntu 20.04 LTS server

#makes sure all mirros and packages are up to date
sudo apt update && sudo apt upgrade

#checks for packages and installs them
if [ ! -f /usr/sbin/unattended-upgrades ]; then
	sudo apt install -y unattended-upgrades
fi

if [ ! -f /usr/sbin/fail2ban ]; then
        sudo apt install -y fail2ban
fi

if [ ! -f /usr/sbin/ufw ]; then
        sudo apt install -y ufw
fi


#enabels installed packages

#enables ufw and limits ports
sudo ufw limit 22/tcp
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw enable
#does global blocks for ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing

#enbale fail2ban
sudo systemctl enable fail2ban
sudo systemctl start fail2ban

#enables unattended-upgrades
sudo dpkg-reconfigure -plow unattended-upgrades

echo "Complete, you will need to adjust the config files for fail2band in /etc/fail2ban/"
echo "Adjust /etc/apt/apt.conf.d/50unattended-upgrades file for unattended-upgrades to your needs"

